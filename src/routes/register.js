export default {
  path: "/register",
  name: "register",
  component: () => import("@/views/register.vue"),
  meta: {
    resumeSession: true,
  },
};
