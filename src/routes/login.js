export default {
  path: "/login",
  name: "login",
  component: () => import("@/views/login.vue"),
  meta: {
    resumeSession: true,
  },
};
