import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";

import home from "./home";
import login from "./login";
import register from "./register";
import verify from "./verify";
import notfound from "./notfound";

Vue.use(VueRouter);

const routes = [home, login, register, verify, notfound];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// Navigation guard to check if route requires authentication
router.beforeEach((to, from, next) => {
  // Check if token is present in localStorage before proceeding to
  // routes that requires authorization and get user details
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("token")) {
      // Retrieve and save user details if not yet available
      // and check if user is verified and allowed to that route
      if (
        store.state &&
        store.state.profile &&
        !store.state.profile.userDetails
      ) {
        store.dispatch("profile/getUserDetails", to.name);
      } else {
        // No need to get user details if userDetails was retrieved before and
        // store hasn't been reset. Check if user is verified and allowed to that route
        store.dispatch("profile/checkIfVerified", to.name);
      }
      next();
      return;
    }
    next("/login");
  } else if (to.matched.some((record) => record.meta.resumeSession)) {
    // If a previous session was detected, redirect user to home page
    if (localStorage.getItem("token")) {
      next("/");
      return;
    }
  }
  next();
});

export default router;
