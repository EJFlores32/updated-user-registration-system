export default {
  path: "/verify",
  name: "verify",
  component: () => import("@/views/verify.vue"),
  meta: {
    requiresAuth: true,
  },
};
