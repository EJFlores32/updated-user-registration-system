export default {
  path: "*",
  name: "404 Page",
  component: () => import("@/views/notfound.vue"),
};
