import Vue from "vue";
import Vuex from "vuex";

import alert from "./modules/alert";
import auth from "./modules/auth";
import profile from "./modules/profile";
import verify from "./modules/verify";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    alert,
    auth,
    profile,
    verify,
  },
});
