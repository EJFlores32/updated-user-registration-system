import Vue from "vue";
import axios from "axios";

const api = axios.create({
  baseURL: "https://baseplate-api.appetiserdev.tech/api/v1",
});

api.interceptors.request.use(
  function (config) {
    const accessToken = localStorage.getItem("token");
    // Check if there was a previous token saved and save previous token to
    // header for succeeding requests
    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

Vue.prototype.$api = api;

export default api;
